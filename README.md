<!-- Gabriel Simas -->
# 👋 Olá, eu sou Gabriel Simas.

> "Assim como um pintor transforma pinceladas em obras-primas e um músico entrelaça notas em sinfonias, um programador transcende simples linhas de código para criar um espetáculo de lógica e criatividade, onde a programação se revela como a verdadeira arte da era digital."  
> — Chat GPT

## 📚 Sobre Mim

Sou um entusiasta do Desenvolvimento Web, Automação Web, QA, AI, Game Dev e tudo que tenha relação com tecnologia. Atualmente, estudo Análise e Desenvolvimento de Sistemas na UCPEL ([Universidade Católica de Pelotas](https://ucpel.edu.br/)), trabalho na empresa Compass UOL, trata-se de um estágio em um Programa de Bolsas de QA e Automação de Testes com Ruby. Estou sempre em busca de aprender e compartilhar conhecimentos!

## 🛠️ Tecnologias e Ferramentas

Aqui estão algumas das tecnologias e ferramentas com as quais tenho experiência:

- Linguagens de Programação: JS, Ruby, Python, Java, C, Gherkin.
- Outras Tencnologias: HTML, CSS, SitePrism, Capybara.
- Ferramentas: Git, GitHub, Cucumber, VS Code, XAMPP, Wordpress.

## 🌱 Atualmente Estou Aprendendo

Estou focando em aprimorar minhas habilidades em desenvolvimento web, automação de testes.

## 💼 Projetos Destacados

Aqui estão alguns dos projetos que eu fiz ao decorrer do Programa de Bolsas da Compass:

- [PB Sprint 4](https://github.com/Gabriel-Simas/RealityStone_-Gabriel_Roberto-_Compass): Ruby e Versionamento de Código.
- [PB Sprint 5](https://github.com/Gabriel-Simas/PB_Sprint-5_-Gabriel_Roberto-_Compass): Automação Web com Ruby e Cucumber.

## 📫 Como Entrar em Contato

Sinta-se à vontade para me contatar:

- LinkedIn: [Gabriel Simas](https://www.linkedin.com/in/gabriel-simas-roberto-da-silva-865430266/)

## 🎯 Objetivo

Meu objetivo é aprofundar e expandir meus conhecimentos a um ponto em que eu possa servir de referência ou contribuir significativamente em projetos na área.

Obrigado por visitar o meu perfil! 😄
